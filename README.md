What I understand so far about Polymorphism, Assertions, Aliasing, and Cloning. 

Polymorphism 
{ I understand it in two ways.Way number one, I think something is a polymorphic class if it has multiple inheritances. 
If for instance we have:

public interface vegan{} 
public class animal{}
public class rabbit extends animal implements vegan{}


I also think polymorphism could be taken advantage of when writing code to make it more efficient and simple. I vaguely remember the programming specifics. 
}


Assertion 
{
All I know is that you have a function "assert" to test the valitidy of a function. If the number is less than a number you want asserting that variable 
will throw back an error. 
}

Aliasing 
{
All I know is that it is very hard to debug and can get incredibly confusing to program with it. 
}

Cloning 
{
I don't believe we ever looked into it in 141 however I tried looking it up online and it seems as if it creates a clone of an object but I feel like
there might be strings attached. It appears as if it makes an exact copy of an object without the extra code needed to remake the object, almost like a
shortcut. I guess I need to search more into it. I do know it is supposed to save time though.  

We use clone() method 

While reading about there was a creepy picture of a bunch of babies being cloned. Seems taken straight out of a futuristic horror film.
}